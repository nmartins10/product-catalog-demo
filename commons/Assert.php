<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Assert
 *
 * @author Nuno
 */
abstract class Assert {

    /**
     * Assert a boolean expression, throwing <code>InvalidArgumentException</code>
     * if the test result is <code>false</code>.
     * @param boolean $expression a boolean expression
     * @param string $optionalMessage the exception message to use if the assertion fails
     * @throws InvalidArgumentException if expression is <code>false</code>
     */
    public static function isTrue($expression, $optionalMessage = null) {
        if ($optionalMessage == null) {
            $optionalMessage = "[Assertion failed] - this expression must be true";
        }
        if (!$expression) {
            throw new InvalidArgumentException($optionalMessage);
        }
    }

    /**
     * Assert that an object is <code>null</code> .
     * @param mixed $object the object to check
     * @param string $optionalMessage the exception message to use if the assertion fails
     * @throws InvalidArgumentException if the object is not <code>null</code>
     */
    public static function isNull($object, $optionalMessage = null) {
        if ($optionalMessage == null) {
            $optionalMessage = "[Assertion failed] - the object parameter must be null";
        }
        if (!is_null($object)) {
            throw new InvalidArgumentException($optionalMessage);
        }
    }

    /**
     * Assert that an object is not <code>null</code> .
     * @param mixed $object the object to check
     * @param string $optionalMessage the exception message to use if the assertion fails
     * @throws InvalidArgumentException if the object is <code>null</code>
     */
    public static function notNull($object, $optionalMessage = null) {
        if ($optionalMessage == null) {
            $optionalMessage = "[Assertion failed] - the object parameter is required; it must not be null";
        }
        if (is_null($object)) {
            throw new InvalidArgumentException($optionalMessage);
        }
    }

    /**
     * Assert that the given String is not empty; that is,
     * it must not be <code>null</code> and not the empty String.
     * @param type $text the String to check
     * @param string $optionalMessage the exception message to use if the assertion fails
     * @throws InvalidArgumentException
     */
    public static function hasLength($text, $optionalMessage = null) {
        if ($optionalMessage == null) {
            $optionalMessage = "[Assertion failed] - this String parameter must have length; it must not be null or empty";
        }

        if (is_null($text) || strlen($text) <= 0) {
            throw new InvalidArgumentException($optionalMessage);
        }
    }

    /**
     * Assert that the given String has valid text content; that is, it must not
     * be <code>null</code> and must contain at least one non-whitespace character.
     * @param string $text the String to check
     * @param string $optionalMessage the exception message to use if the assertion fails
     * @throws InvalidArgumentException
     */
    public static function hasText($text, $optionalMessage = null) {
        if ($optionalMessage == null) {
            $optionalMessage = "[Assertion failed] - this String argument must have text; it must not be null, empty, or blank";
        }

        if (is_null($text) || strlen($text) <= 0 || ctype_space($text)) {
            throw new InvalidArgumentException($optionalMessage);
        }
    }

}

?>
