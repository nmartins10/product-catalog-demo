<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of EntityUtil
 *
 * @author Nuno
 */
abstract class EntityUtil {
    
	/**
	 * Formats a Date to the format 'Y-m-d H:i:s'.
	 * @param DateTime $date the date to be formatted.
	 * @return the formatted date or null in case of the parameter is null.
	 */
    public static function formatDateTimeToDatabase($date) {
        if($date != null) {
    		return date_format($date, "Y-m-d H:i:s");
        } else {
        	return null;
        }
    }
}

?>
