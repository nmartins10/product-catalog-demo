<?php

/**
 *
 * @author Nuno
 */
interface LoginDao {
    
	/**
	 * Validates if the combination of email and password is valid.
	 * @param string $email the email to be validated.
	 * @param string $passwordHash the password to be validated.
	 * @return the id of the user if the validation succeeds or false otherwise.
	 */
    public function validateLogin($email, $passwordHash);

}

?>
