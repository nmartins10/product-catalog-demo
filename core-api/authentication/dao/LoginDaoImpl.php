<?php
require_once 'LoginDao.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/database/service/DatabaseServiceImpl.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/authentication/domain/UserStatus.php';

/**
 * Description of LoginDaoImpl
 *
 * @author Nuno
 */
class LoginDaoImpl implements LoginDao {

    private $connection;
    private $databaseService;

    public function validateLogin($email, $passwordHash) {
        $query = "SELECT id FROM user WHERE email = BINARY :email AND password = :password AND status = :status";
        $stmt = $this->connection->prepare($query);
        $stmt->execute(
                array(':email' => $email,
                    ':password' => $passwordHash,
                    ':status' => UserStatus::ACTIVE)
        );

        $row = $stmt->fetch();
        if ($row == null) {
            return false;
        }
        return $row['id'];
    }

    private static $instance;

    private function __construct() {
        $this->databaseService = DatabaseServiceImpl::getInstance();
        $this->connection = $this->databaseService->getConnection();
    }

    public static function getInstance() {
        if (!isset(self::$instance)) {
            self::$instance = new LoginDaoImpl();
        }
        return self::$instance;
    }

}

?>
