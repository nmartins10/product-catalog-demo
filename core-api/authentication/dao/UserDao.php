<?php

/**
 *
 * @author Nuno
 */
interface UserDao {
    
	/**
	 * Finds a user by it's id.
	 * @param int $userId the user id.
	 * @return the user or null if not found.
	 */
    public function findUserById($userId);
    
    /**
     * Finds a user by it's email.
     * @param string $email the user email.
     * @return the user or null if not found.
     */
    public function findUserByEmail($email);
    
    /**
     * Searches for users matching the following parameters:
     * @param string $email user email
     * @param string $name user name
     * @param UserType $type user type
     * @param UserStatus $status user status
     * @param DateTime $fromCreateDt
     * @param DateTime $toCreateDt
     * @param DateTime $fromLastLoginDt
     * @param DateTime $toLastLoginDt
     * @return the users that matches the search parameters.
     */
    public function searchUsers($email, $name, $type, $status, $fromCreateDt, $toCreateDt, $fromLastLoginDt, $toLastLoginDt);
    
    /**
     * Creates a new user.
     * @param UserEntity $user the user to be created.
     * @param string $passwordHash the password for the new user.
	 * @return the created user or null if not created.
     */
    public function createUser(UserEntity $user, $passwordHash);

    /**
     * Updates an existing user.
     * @param \UserEntity $user the user.
     * @return the updated user (same as the parameter) or null if not updated.
     */
    public function updateUser(\UserEntity $user);
    
    /**
     * Updates the user password.
     * @param int $userId the user id.
     * @param string $newPassword the new password.
     * @return true in success, false otherwise.
     */
    public function updateUserPassword($userId, $newPassword);
}

?>
