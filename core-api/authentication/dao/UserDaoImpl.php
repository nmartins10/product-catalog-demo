<?php
require_once 'UserDao.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/authentication/util/UserUtil.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/database/service/DatabaseServiceImpl.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/commons/EntityUtil.php';

/**
 * Description of UserDaoImpl
 *
 * @author Nuno
 */
class UserDaoImpl implements UserDao {
    private $connection;
    private $databaseService;

    public function findUserById($userId) {
        $query = "SELECT * FROM user WHERE id = :id";
        $stmt = $this->connection->prepare($query);
        $stmt->execute(array(
            ':id' => $userId));
        
        $row = $stmt->fetch();
        if ($row == null) {
            return null;
        }
        
        return UserUtil::convertUserFromDatabase($row);
    }

    public function findUserByEmail($email) {
        $query = "SELECT * FROM user WHERE email = :email";
        $stmt = $this->connection->prepare($query);
        $stmt->execute(array(
            ':email' => $email));
        
        $row = $stmt->fetch();
        if ($row == null) {
            return null;
        }
        
        return UserUtil::convertUserFromDatabase($row);
    }

    public function searchUsers($email, $name, $type, $status, $fromCreateDt, $toCreateDt, $fromLastLoginDt, $toLastLoginDt) {
        $query = "SELECT * FROM user WHERE 1=1";
        
        if (!empty($email)) {
            $query .= " AND email like '%$email%'";
        }
        if (!empty($name)) {
            $query .= " AND name like '%$name%'";
        }
        if (!empty($type)) {
            $query .= " AND type = '$type'";
        }
        if (!empty($status)) {
            $query .= " AND status = '$status'";
        }
        if (!empty($fromCreateDt)) {
            $query .= " AND create_dt >= '" . EntityUtil::formatDateTimeToDatabase($fromCreateDt) . "'";
        }
        if (!empty($toCreateDt)) {
            $query .= " AND create_dt <= '" . EntityUtil::formatDateTimeToDatabase($toCreateDt) . "'";
        }
        if (!empty($fromLastLoginDt)) {
            $query .= " AND last_login_dt >= '" . EntityUtil::formatDateTimeToDatabase($fromLastLoginDt) . "'";
        }
        if (!empty($toLastLoginDt)) {
            $query .= " AND last_login_dt <= '" . EntityUtil::formatDateTimeToDatabase($toLastLoginDt) . "'";
        }
        
        $users = array();
        foreach ($this->connection->query($query) as $row) {
            $users[] = UserUtil::convertUserFromDatabase($row);
        }
        return $users;
    }

    public function createUser(\UserEntity $user, $passwordHash) {
        $query = "INSERT INTO user(email, password, name, type, status, create_dt, last_modified_dt) VALUES(:email, :password, :name, :type, :status, :create_dt, :last_modified_dt)";
        $stmt = $this->connection->prepare($query);
        $result = $stmt->execute(
            array(
                ':email' => $user->getEmail(),
                ':password' => $passwordHash,
                ':name' => $user->getName(),
                ':type' => $user->getType(),
                ':status' => $user->getStatus(),
                ':create_dt' => EntityUtil::formatDateTimeToDatabase($user->getCreateDt()),
                ':last_modified_dt' => EntityUtil::formatDateTimeToDatabase($user->getLastModifiedDt())));
        
        if ($result == FALSE) {
            return null;
        }
        $user->setId($this->connection->lastInsertId());
        return $user;
    }

    public function updateUser(\UserEntity $user) {
        $query = "UPDATE user SET email = :email, name = :name, type = :type, status = :status, create_dt = :create_dt, last_modified_dt = :last_modified_dt, last_login_dt = :last_login_dt, recovery_code = :recovery_code, recovery_code_valid_until = :recovery_code_valid_until WHERE id = :id";
        $stmt = $this->connection->prepare($query);
        $result = $stmt->execute(
            array(
                ':email' => $user->getEmail(),
                ':name' => $user->getName(),
                ':type' => $user->getType(),
                ':status' => $user->getStatus(),
                ':create_dt' => EntityUtil::formatDateTimeToDatabase($user->getCreateDt()),
                ':last_modified_dt' => EntityUtil::formatDateTimeToDatabase($user->getLastModifiedDt()),
                ':last_login_dt' => EntityUtil::formatDateTimeToDatabase($user->getLastLoginDt()),
                ':recovery_code' => $user->getRecoveryCode(),
                ':recovery_code_valid_until' => EntityUtil::formatDateTimeToDatabase($user->getRecoveryCodeValidUntil()),
                ':id' => $user->getId()));
        return $result; // TODO: alterar para retornar ou o user ou null.
    }

    public function updateUserPassword($userId, $newPassword) {
        $query = "UPDATE user SET password = :password WHERE id = :id";
        $stmt = $this->connection->prepare($query);
        $result = $stmt->execute(array(
            ':password' => $newPassword,
            ':id' => $userId));
        return $result; // TODO: alterar para retornar explicitamente true ou false.
    }
    private static $instance;

    private function __construct() {
        $this->databaseService = DatabaseServiceImpl::getInstance();
        $this->connection = $this->databaseService->getConnection();
    }

    public static function getInstance() {
        if (!isset(self::$instance)) {
            self::$instance = new UserDaoImpl();
        }
        return self::$instance;
    }
}

?>
