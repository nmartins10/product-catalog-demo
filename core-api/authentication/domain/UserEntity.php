<?php
require_once 'UserStatus.php';
require_once 'UserType.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/session/util/SessionUtil.php';

class UserEntity {
    private $id;
    private $email;
    private $name;
    private $type;
    private $status;
    private $createDt;
    private $lastModifiedDt;
    private $lastLoginDt;
    private $recoveryCode;
    private $recoveryCodeValidUntil;
    
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getType() {
        return $this->type;
    }

    public function setType($type) {
        $this->type = $type;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function getCreateDt() {
        return $this->createDt;
    }

    public function setCreateDt(DateTime $createDt) {
        $this->createDt = $createDt;
    }

    public function getLastModifiedDt() {
        return $this->lastModifiedDt;
    }

    public function setLastModifiedDt(DateTime $lastModifiedDt) {
        $this->lastModifiedDt = $lastModifiedDt;
    }

    public function getLastLoginDt() {
        return $this->lastLoginDt;
    }

    public function setLastLoginDt(DateTime $lastLoginDt) {
        $this->lastLoginDt = $lastLoginDt;
    }
    
    public function getRecoveryCode() {
    	return $this->recoveryCode;
    }
    
    public function setRecoveryCode($recoveryCode) {
    	$this->recoveryCode = $recoveryCode;
    }

    public function getRecoveryCodeValidUntil() {
    	return $this->recoveryCodeValidUntil;
    }
    
    public function setRecoveryCodeValidUntil(DateTime $recoveryCodeValidUntil) {
    	$this->recoveryCodeValidUntil = $recoveryCodeValidUntil;
    }
    
    public function __toString() {
        return UserUtil::userToString($this);
    }
}

?>
