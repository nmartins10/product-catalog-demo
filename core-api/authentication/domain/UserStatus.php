<?php

/**
 * Description of UserStatus
 *
 * @author Nuno
 */
abstract class UserStatus {
    const ACTIVE = 'ACTIVE';
    const DELETED = 'DELETED';
    const BLOCKED = 'BLOCKED';
}

?>
