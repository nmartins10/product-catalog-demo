<?php
require_once 'bean/Credentials.php';

/**
 *
 * @author Nuno
 */
interface AuthenticationService {
    
    public function authenticate(Credentials $credentials);

    public function logout($sessionId, $closeType);

}

?>
