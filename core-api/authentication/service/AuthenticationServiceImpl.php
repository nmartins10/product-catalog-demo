<?php

require_once 'AuthenticationService.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/authentication/dao/LoginDaoImpl.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/time/service/LocalSystemTimeService.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/commons/Assert.php';
require_once 'bean/Credentials.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/authentication/domain/UserEntity.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/session/service/SessionServiceImpl.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/session/domain/SessionEntity.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/authentication/service/bean/Authentication.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/authentication/service/UserManagementServiceImpl.php';

/**
 * Description of LoginServiceImpl
 *
 * @author Nuno
 */
class AuthenticationServiceImpl implements AuthenticationService {

    private $timeService;
    private $loginDao;
    private $sessionService;
    private $userManagementService;
    private $log;

    public function authenticate(Credentials $credentials) {
        $this->log->info("{email=" . $credentials->getEmail() . "}");
        Assert::notNull($credentials, "credentials are required and must not be null");
        Assert::hasText($credentials->getEmail(), "email is required and must have content");
        Assert::hasText($credentials->getPasswordHash(), "password is required and must have content");

        $userId = $this->loginDao->validateLogin($credentials->getEmail(), $credentials->getPasswordHash());
        $successfulAuthentication = $userId != null ? true : false;

        $authentication = null;
        if ($successfulAuthentication == true) {
            $user = $this->userManagementService->getUser($userId);
            $user->setLastLoginDt($this->timeService->getCurrentDateTime());
            $this->userManagementService->updateUser($user);
            $session = $this->sessionService->openSession($userId);
            $authentication = new Authentication();
            $authentication->setUser($user);
            $authentication->setSession($session);
            $this->log->info("successful authenticated with user[$user] and session[$session]");
        }

        return $authentication;
    }

    public function logout($sessionId, $closeType) {
        $this->log->info("{sessionId=$sessionId, closeType=$closeType}");
        //ver se faz sentido pesquisar antes se a sessão existe e se já foi fechada. Se sim, o que fazer??
        if(empty($closeType)) {
            $closeType = SessionCloseType::EXPIRED;
        }
        $this->sessionService->closeSession($sessionId, $closeType);
    }

    private static $instance;

    private function __construct() {
        $this->timeService = LocalSystemTimeService::getInstance();
        $this->loginDao = LoginDaoImpl::getInstance();
        $this->sessionService = SessionServiceImpl::getInstance();
        $this->userManagementService = UserManagementServiceImpl::getInstance();
        $this->log = Logger::getLogger("log");
    }

    public static function getInstance() {
        if (!isset(self::$instance)) {
            self::$instance = new AuthenticationServiceImpl();
        }
        return self::$instance;
    }

}

?>
