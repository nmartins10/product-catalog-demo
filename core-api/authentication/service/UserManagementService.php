<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/authentication/domain/UserType.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/authentication/domain/UserEntity.php';
require_once 'bean/UserSearchCriteria.php';

/**
 *
 * @author Nuno
 */
interface UserManagementService {
    
    public function getUser($userId);
    
    public function searchUsers(UserSearchCriteria $searchCriteria);
    
    public function createUser($email, $passwordHash, $name, UserType $type);
    
    public function updateUser(UserEntity $user);
    
    public function deleteUser($userId);
    
    public function blockUser($userId);
    
    public function unblockUser($userId);
    
    public function userHasValidPasswordRecovery($email);
    
    public function recoverPassword($email);
    
    public function confirmPasswordRecovery($email, $recoveryCode, $newPasswordHash);
    
}

?>
