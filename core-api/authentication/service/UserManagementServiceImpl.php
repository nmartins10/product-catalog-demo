<?php
require_once 'UserManagementService.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/authentication/dao/UserDaoImpl.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/mail/service/MailServiceImpl.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/mail/service/bean/Mail.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/time/service/LocalSystemTimeService.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/authentication/service/bean/UserNotFoundException.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/authentication/service/bean/UserAlreadyExistsException.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/authentication/service/bean/InvalidPasswordRecoveryException.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/authentication/service/bean/ErrorStartingPasswordRecoveryException.php';

/**
 * Description of UserManagementServiceImpl
 *
 * @author Nuno
 */
class UserManagementServiceImpl implements UserManagementService {
    private static $DEFAULT_RECOVERY_CODE_PERIOD = 'PT1H';
    private $timeService;
    private $mailService;
    private $userDao;
    private $log;

    public function blockUser($userId) {
        $this->log->info("{userId=$userId}");
        $user = $this->userDao->findUserById($userId);
        if ($user == null || $user->getStatus() == UserStatus::DELETED) {
            $ex = new UserNotFoundException($userId);
            $this->log->error($ex);
            throw $ex;
        }
        
        $user->setStatus(UserStatus::BLOCKED);
        $user->setLastModifiedDt($this->timeService->getCurrentDateTime());
        
        $result = $this->userDao->updateUser($user);
        return $result == null ? false : true;
    }

    public function createUser($email, $passwordHash, $name, \UserType $type) {
        $this->log->info("{email=$email, name=$name, type=$type}");
        Assert::hasText($email, "email is required and must have content");
        Assert::hasText($passwordHash, "password is required and must have content");
        Assert::hasText($name, "name is required and must have content");
        Assert::notNull($type, "type is required and must be set"); // ver se funciona assim
        
        $user = $this->userDao->findUserByEmail($email);
        if ($user != null) {
            $ex = new UserAlreadyExistsException($email);
            $this->log->error($ex);
            throw $ex;
        }
        
        $now = $this->timeService->getCurrentDateTime();
        
        $user = new User();
        $user->setEmail($email);
        $user->setName($name);
        $user->setType($type);
        $user->setStatus(UserStatus::ACTIVE);
        $user->setCreateDt($now);
        $user->setLastModifiedDt($now);
        
        return $this->userDao->createUser($user, $passwordHash);
    }

    public function deleteUser($userId) {
        $this->log->info("{userId=$userId}");
        $user = $this->userDao->findUserById($userId);
        if ($user == null || $user->getStatus() == UserStatus::DELETED) {
            $ex = new UserNotFoundException($userId);
            $this->log->error($ex);
            throw $ex;
        }
        
        $user->setStatus(UserStatus::DELETED);
        $user->setLastModifiedDt($this->timeService->getCurrentDateTime());
        
        $result = $this->userDao->updateUser($user);
        return $result == null ? false : true;
    }

    public function getUser($userId) {
        $this->log->info("{userId=$userId}");
        $user = $this->userDao->findUserById($userId);
        if ($user == null || $user->getStatus() == UserStatus::DELETED) {
            $ex = new UserNotFoundException($userId);
            $this->log->error($ex);
            throw $ex;
        }
        return $user;
    }

    public function searchUsers(\UserSearchCriteria $searchCriteria) {
        $this->log->info("{searchCriteria=$searchCriteria}");
        return $this->userDao->searchUsers(
            $searchCriteria->getEmail(), 
            $searchCriteria->getName(), 
            $searchCriteria->getType(), 
            $searchCriteria->getStatus(), 
            $searchCriteria->getFromCreateDt(), 
            $searchCriteria->getToCreateDt(), 
            $searchCriteria->getFromLastLoginDt(), 
            $searchCriteria->getToLastLoginDt());
    }

    public function unblockUser($userId) {
        $this->log->info("{userId=$userId}");
        $user = $this->userDao->findUserById($userId);
        if ($user == null || $user->getStatus() == UserStatus::DELETED) {
            $ex = new UserNotFoundException($userId);
            $this->log->error($ex);
            throw $ex;
        }
        
        $user->setStatus(UserStatus::ACTIVE);
        $user->setLastModifiedDt($this->timeService->getCurrentDateTime());
        
        $result = $this->userDao->updateUser($user);
        return $result == null ? false : true;
    }

    public function updateUser(\UserEntity $user) {
        $this->log->info("{user=$user}");
        $user = $this->userDao->findUserById($user->getId());
        if ($user == null || $user->getStatus() == UserStatus::DELETED) {
            $ex = new UserNotFoundException($user->getId());
            $this->log->error($ex);
            throw $ex;
        }
        
        $user->setLastModifiedDt($this->timeService->getCurrentDateTime());
        
        $result = $this->userDao->updateUser($user);
        return $result == null ? false : true;
    }

    public function userHasValidPasswordRecovery($email) {
        $this->log->info("{email=$email}");
        Assert::hasText($email, "email is required and must have content");
        
        $user = $this->userDao->findUserByEmail($email);
        if ($user == null || $user->getRecoveryCode() == null || $this->hasRecoveryCodeExpired($user) === true) {
            return false;
        }
        return true;
    }

    public function recoverPassword($email) {
        $this->log->info("{email=$email}");
        Assert::hasText($email, "email is required and must have content");
        
        // Validar que o utilizador existe
        $user = $this->userDao->findUserByEmail($email);
        if ($user == null || $user->getStatus() == UserStatus::DELETED) {
            $ex = new UserNotFoundException($email);
            $this->log->error($ex);
            throw $ex;
        }
        
        // gerar c�digo para recuperar a password
        $passwordRecoveryCode = $this->generateCodeForPasswordRecovery($email);
        
        $now = $this->timeService->getCurrentDateTime();
        $recoveryCodeValidUntil = $this->timeService->getCurrentDateTime();
        $recoveryCodeValidUntil->add(new DateInterval(self::$DEFAULT_RECOVERY_CODE_PERIOD));
        
        $user->setRecoveryCode($passwordRecoveryCode);
        $user->setRecoveryCodeValidUntil($recoveryCodeValidUntil);
        $user->setLastModifiedDt($now);
        if ($this->userDao->updateUser($user) == null) {
            $ex = new ErrorStartingPasswordRecoveryException();
            $this->log->error($ex);
            throw $ex;
        }
        
        $mail = $this->buildEmailForPasswordRecovery($email, $passwordRecoveryCode);
        $this->mailService->sendEmail($mail);
        
        return true;
    }

    public function confirmPasswordRecovery($email, $recoveryCode, $newPasswordHash) {
        Assert::hasText($email, 'email is required and must have content');
        Assert::hasText($recoveryCode, 'recovery code is required and must have content');
        Assert::hasText($newPasswordHash, 'password is required and must have content');
        
        $user = $this->userDao->findUserByEmail($email);
        if ($user == null || $user->getStatus() == UserStatus::DELETED) {
            $ex = new UserNotFoundException($email);
            $this->log->error($ex);
            throw $ex;
        }
        
        if ($user->getRecoveryCode() == null || empty($user->getRecoveryCode()) || $this->hasRecoveryCodeExpired($user) ||
             strcmp($user->getRecoveryCode(), $recoveryCode) != 0) {
            $ex = new InvalidPasswordRecoveryException();
            $this->log->error($ex);
            throw $ex;
        }
        
        // Tudo validado?? vamos alterar a password e expirar o c�digo
        $now = $this->timeService->getCurrentDateTime();
        $user->setLastModifiedDt($now);
        $user->setRecoveryCodeValidUntil($now);
        
        if ($this->userDao->updateUserPassword($user->getId(), $newPasswordHash) == null) {
            return false;
        }
        
        if ($this->userDao->updateUser($user) == null) {
            return false;
        }
        
        return true;
    }

    private function hasRecoveryCodeExpired(UserEntity $user) {
        if ($user->getRecoveryCodeValidUntil() < $this->timeService->getCurrentDateTime()) {
            return true;
        } else {
            return false;
        }
    }

    private function generateCodeForPasswordRecovery($email) {
        return md5(uniqid($email, true));
    }

    private function buildEmailForPasswordRecovery($email, $passwordRecoveryCode) {
        $mail = new Mail();
        $mail->setFrom("nuno@nmartins.net");
        $mail->setFromName("nmartins.net");
        $mail->setMessage($passwordRecoveryCode);
        $mail->setSubject("Password Recovery");
        $mail->addToAddress($email);
        return $mail;
    }
    private static $instance;

    private function __construct() {
        $this->log = Logger::getLogger("log");
        $this->timeService = LocalSystemTimeService::getInstance();
        $this->mailService = MailServiceImpl::getInstance();
        $this->userDao = UserDaoImpl::getInstance();
    }

    public static function getInstance() {
        if (!isset(self::$instance)) {
            self::$instance = new UserManagementServiceImpl();
        }
        return self::$instance;
    }
}

?>
