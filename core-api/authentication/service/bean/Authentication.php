<?php

/**
 * Description of LoginAuthentication
 *
 * @author Nuno
 */
class Authentication {
    private $user;
    private $session;
    
    public function getUser() {
        return $this->user;
    }

    public function setUser($user) {
        $this->user = $user;
    }

    public function getSession() {
        return $this->session;
    }

    public function setSession($session) {
        $this->session = $session;
    }


}

?>
