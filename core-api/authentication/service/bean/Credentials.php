<?php

/**
 * Description of Credentials
 *
 * @author Nuno
 */
class Credentials {
    
    private $email;
    private $passwordHash;
    
    public function __construct($email, $passwordHash) {
        $this->email = $email;
        $this->passwordHash = $passwordHash;
    }
    
    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getPasswordHash() {
        return $this->passwordHash;
    }

    public function setPasswordHash($passwordHash) {
        $this->passwordHash = $passwordHash;
    }


}

?>
