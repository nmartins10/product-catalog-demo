<?php

/**
 * Description of UserAlreadyExistsException
 *
 * @author Nuno
 */
class UserAlreadyExistsException extends Exception {
    
    private $email;
    
    public function __construct($email) {
        parent::__construct("email: " . $email);
        $this->email = $email;
    }
    
    public function getEmail() {
        return $this->email;
    }
}

?>
