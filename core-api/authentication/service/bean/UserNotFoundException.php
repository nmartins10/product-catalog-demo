<?php

/**
 * Description of UserNotFoundException
 *
 * @author Nuno
 */
class UserNotFoundException extends Exception {
    
    private $userId;
    
    public function __construct($userId) {
        parent::__construct("userId: " . $userId);
        $this->userId = $userId;
    }
    
    public function getUserId() {
        return $this->userId;
    }
}

?>
