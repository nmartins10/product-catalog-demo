<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/authentication/domain/UserType.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/authentication/domain/UserStatus.php';

/**
 * Description of UserSearchCriteria
 *
 * @author Nuno
 */
class UserSearchCriteria {
    private $email;
    private $name;
    private $type;
    private $status;
    private $fromCreateDt;
    private $toCreateDt;
    private $fromLastLoginDt;
    private $toLastLoginDt;
    
    public function getEmail() {
        return $this->email;
    }

    public function setEmail($email) {
        $this->email = $email;
    }

    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }

    public function getType() {
        return $this->type;
    }

    public function setType(UserType $type) {
        $this->type = $type;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setStatus(UserStatus $status) {
        $this->status = $status;
    }

    public function getFromCreateDt() {
        return $this->fromCreateDt;
    }

    public function setFromCreateDt(DateTime $fromCreateDt) {
        $this->fromCreateDt = $fromCreateDt;
    }

    public function getToCreateDt() {
        return $this->toCreateDt;
    }

    public function setToCreateDt(DateTime $toCreateDt) {
        $this->toCreateDt = $toCreateDt;
    }

    public function getFromLastLoginDt() {
        return $this->fromLastLoginDt;
    }

    public function setFromLastLoginDt(DateTime $fromLastLoginDt) {
        $this->fromLastLoginDt = $fromLastLoginDt;
    }

    public function getToLastLoginDt() {
        return $this->toLastLoginDt;
    }

    public function setToLastLoginDt(DateTime $toLastLoginDt) {
        $this->toLastLoginDt = $toLastLoginDt;
    }

    public function __toString() {
        return UserUtil::searchCriteriaToString($this);
    }
}

?>
