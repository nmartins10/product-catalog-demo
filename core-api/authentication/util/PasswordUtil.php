<?php

abstract class PasswordUtil {
    
    private static $SALT = "�~^%|!#)=?�@�-_$;{[]}�+*�`&(.:,��";
    
    public static function hashPassword($password) {
        return sha1($password . self::$SALT);
    }
    
}

?>