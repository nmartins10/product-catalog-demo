<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/authentication/domain/UserEntity.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/authentication/service/bean/UserSearchCriteria.php';

/**
 * Description of UserUtil
 *
 * @author Nuno
 */
abstract class UserUtil {

    public static function convertUserFromDatabase($databaseRow) {
        $user = new Userentity();
        $user->setId($databaseRow['id']);
        $user->setEmail($databaseRow['email']);
        $user->setName($databaseRow['name']);
        $user->setType($databaseRow['type']);
        $user->setStatus($databaseRow['status']);
        $user->setCreateDt(new DateTime($databaseRow['create_dt']));
        if ($databaseRow['last_modified_dt'] != null) {
            $user->setLastModifiedDt(new DateTime($databaseRow['last_modified_dt']));
        }
        if ($databaseRow['last_login_dt'] != null) {
            $user->setLastLoginDt(new DateTime($databaseRow['last_login_dt']));
        }
        $user->setRecoveryCode($databaseRow['recovery_code']);
        if ($databaseRow['recovery_code_valid_until'] != null) {
            $user->setRecoveryCodeValidUntil(new DateTime($databaseRow['recovery_code_valid_until']));
        }
        
        return $user;
    }

    public static function userToString(UserEntity $user) {
        $str = "id=" . $user->getId() . ", email=" . $user->getEmail() . ", name=" . $user->getName() . ", type=" . $user->getType() . ", status=" .
             $user->getStatus() . ",";
        
        if ($user->getCreateDt() != null) {
            $str .= " createDt=" . EntityUtil::formatDateTimeToDatabase($user->getCreateDt()) . ",";
        } else {
            $str .= " createDt=null,";
        }
        
        if ($user->getLastModifiedDt() != null) {
            $str .= " lastModifiedDt=" . EntityUtil::formatDateTimeToDatabase($user->getLastModifiedDt()) . ",";
        } else {
            $str .= " lastModifiedDt=null,";
        }
        
        if ($user->getLastLoginDt() != null) {
            $str .= " lastLoginDt=" . EntityUtil::formatDateTimeToDatabase($user->getLastLoginDt()) . ",";
        } else {
            $str .= " lastLoginDt=null,";
        }
        
        $str .= " recoveryCode=" . $user->getRecoveryCode() . ",";
        
        if ($user->getRecoveryCodeValidUntil() != null) {
            $str .= " recoveryCodeValidUntil=" . EntityUtil::formatDateTimeToDatabase($user->getRecoveryCodeValidUntil()) . ",";
        } else {
            $str .= " recoveryCodeValidUntil=null,";
        }
        
        return $str;
    }

    public static function searchCriteriaToString(UserSearchCriteria $searchCriteria) {
        $str = "email=" . $searchCriteria->getEmail() . ", name=" . $searchCriteria->getName() . ", type=" . $searchCriteria->getType() . ", status=" .
             $searchCriteria->getStatus() . ",";
        
        if ($searchCriteria->getFromCreateDt() != null) {
            $str .= " fromCreateDt=" . EntityUtil::formatDateTimeToDatabase($searchCriteria->getFromCreateDt()) . ",";
        } else {
            $str .= " fromCreateDt=null,";
        }
        
        if ($searchCriteria->getToCreateDt() != null) {
            $str .= " toCreateDt=" . EntityUtil::formatDateTimeToDatabase($searchCriteria->getToCreateDt()) . ",";
        } else {
            $str .= " toCreateDt=null,";
        }
        
        if ($searchCriteria->getFromLastLoginDt() != null) {
            $str .= " fromLastLoginDt=" . EntityUtil::formatDateTimeToDatabase($searchCriteria->getFromLastLoginDt()) . ",";
        } else {
            $str .= " fromLastLoginDt=null,";
        }
        
        if ($searchCriteria->getToLastLoginDt() != null) {
            $str .= " toLastLoginDt=" . EntityUtil::formatDateTimeToDatabase($searchCriteria->getToLastLoginDt()) . ",";
        } else {
            $str .= " toLastLoginDt=null,";
        }
        
        return $str;
    }
}

?>
