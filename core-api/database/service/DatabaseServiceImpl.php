<?php
require_once 'DatabaseService.php';
require_once 'bean/UnableToConnectToDatabaseException.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/commons/log/Logger.php';
Logger::configure($_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/etc/log4php-config.xml');

/**
 * Description of DatabaseConfigurationServiceImpl
 *
 * @author Nuno
 */
class DatabaseServiceImpl implements DatabaseService {

    private static $connection;

    private static $instance;

    private $log;

    public function getConnection() {
        return self::$connection;
    }

    private function __construct() {
        $this->log = Logger::getLogger("log");
        $this->connectToDatabase();
    }

    public static function getInstance() {
        if (!isset(self::$instance)) {
            self::$instance = new DatabaseServiceImpl();
        }
        return self::$instance;
    }

    private function connectToDatabase() {
        $ini_array = parse_ini_file("c:/xampp/htdocs/SalesDemo/etc/db.ini");
        $host = $ini_array['server'];
        $dbname = $ini_array['database'];
        
        try {
            self::$connection = new PDO("mysql:host=$host;dbname=$dbname", $ini_array['username'], $ini_array['password']);
        } catch (PDOException $e) {
            $this->log->fatal($e);
            throw new UnableToConnectToDatabaseException();
        }
        
        // self::$connection = mysql_connect($ini_array['server'], $ini_array['username'], $ini_array['password']) or die("Error connecting to the Database System.");
        // mysql_select_db($ini_array['database']) or die ("Database not found.");
    }
}

?>
