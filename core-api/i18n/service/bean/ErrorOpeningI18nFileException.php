<?php

class ErrorOpeningI18nFileException extends Exception {
    private $filePath;

    public function __construct($filePath) {
        parent::__construct("filePath: " . $filePath);
        $this->filePath = $filePath;
    }

    public function getFilePath() {
        return $this->filePath;
    }
}

?>