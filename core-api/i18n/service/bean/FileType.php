<?php

abstract class FileType {
    const LABELS = 'LABELS';
    const MESSAGES = 'MESSAGES';

    public static function values() {
        return array(
            self::LABELS,
            self::MESSAGES);
    }
}

?>