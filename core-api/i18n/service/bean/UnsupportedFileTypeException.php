<?php

class UnsupportedFileTypeException extends Exception {
    private $fileType;

    public function __construct($fileType) {
        parent::__construct("fileType: " . $fileType);
        $this->fileType = $fileType;
    }

    public function getFileType() {
        return $this->fileType;
    }
}

?>