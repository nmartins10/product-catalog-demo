<?php

interface i18nService {
    
    public function loadI18nFile($filePath, $fileType);
    
    public function getLabel($key);
    
    public function getMessage($key);
    
}

?>