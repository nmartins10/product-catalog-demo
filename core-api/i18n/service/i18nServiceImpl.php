<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/i18n/service/i18nService.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/i18n/service/bean/ErrorOpeningI18nFileException.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/i18n/service/bean/UnsupportedFileTypeException.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/i18n/service/bean/FileType.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/commons/Assert.php';

class i18nServiceImpl implements i18nService {
    private $log;

    public function getLabel($key) {
        Assert::hasText($key, 'key is required and must have content');
        return $_SESSION['labels'][$key];
    }

    public function getMessage($key) {
        Assert::hasText($key, 'key is required and must have content');
        return $_SESSION['messages'][$key];
    }

    public function loadI18nFile($filePath, $fileType) {
        Assert::hasText($filePath, 'file path is required and must have content');
        Assert::notNull($fileType, 'file type is required and must be set');

        if (!in_array($fileType, FileType::values())) {
            $ex = new UnsupportedFileTypeException($fileType);
            $this->log->error($ex);
            throw $ex;
        }
        
        $content = $this->readFile($filePath);
        $this->buildResource($fileType, $content);
    }

    private function buildResource($fileType, $content) {
        switch ($fileType) {
            case FileType::LABELS:
                $_SESSION['labels'] = $content;
                break;
            case FileType::MESSAGES:
                $_SESSION['messages'] = $content;
                break;
        }
    }
    
    private function readFile($filePath) {
        $content = array();
        
        $fp = fopen($filePath, 'r');
        if ($fp == false) {
            $ex = new ErrorOpeningI18nFileException($filePath);
            $this->log->error($ex);
            throw $ex;
        }
        
        while (($line = fgets($fp)) !== false) {
            $keyValue = explode("=", $line);
        
            // Se a linha n�o estiver vazia, nem tiver um # na primeira posi��o (coment�rio),
            // e se o array que foi dividido tiver duas posi��es
            if (!(empty($line) || strcmp($line[0], '#') == 0 || count($keyValue) != 2)) {
                $content[$keyValue[0]] = $keyValue[1];
            }
        }
        
        fclose($fp);
        return $content;
    }
    
    private static $instance;

    private function __construct() {
        $this->log = Logger::getLogger("log");
    }

    public static function getInstance() {
        if (!isset(self::$instance)) {
            self::$instance = new i18nServiceImpl();
        }
        return self::$instance;
    }
}

?>