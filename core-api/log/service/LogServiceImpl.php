<?php
require_once require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/log/service/LogService.php';
require_once require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/log/service/bean/Logger.php';

class LogServiceImpl implements LogService {
    
    private $log;
    
    public function info($message) {
        $this->log->info($message);
    }
    
    private static $instance;
    
    private function __construct() {
        Logger::configure($_SERVER['DOCUMENT_ROOT'] . '/SalesDemo/etc/log4php-config.xml');
        $this->log = Logger::getLogger("log");
    }
    
    public static function getInstance() {
        if (!isset(self::$instance)) {
            self::$instance = new LogServiceImpl();
        }
        return self::$instance;
    }
}

?>