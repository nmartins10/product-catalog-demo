<?php
require_once 'bean/Mail.php';

interface MailService {
	
	public function sendEmail(Mail $mail);
	
}

?>