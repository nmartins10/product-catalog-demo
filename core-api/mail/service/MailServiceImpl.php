<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/commons/Assert.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/mail/service/bean/php-mailer/class.phpmailer.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/mail/service/bean/ErrorSendingEmailException.php';
require_once 'MailService.php';

class MailServiceImpl implements MailService {
    private $log;
    private $charSet;

    public function sendEmail(Mail $mail) {
        $this->log->info("{mail=$mail}");
        Assert::notNull($mail, "mail is required and must not be null");
        Assert::isTrue(count($mail->getToAddresses()) >= 1, "you must set at least one email address");
        
        $mailer = new PHPMailer();
        $mailer->CharSet = $this->charSet;
        $mailer->isSendmail();
        $mailer->setFrom($mail->getFrom(), $mail->getFromName());
        
        foreach ($mail->getToAddresses() as $key => $value) {
            $mailer->addAddress($value);
        }
        
        if (count($mail->getCcAddresses()) >= 1) {
            foreach ($mail->getCcAddresses() as $address) {
                $mailer->addCC($address);
            }
        }
        
        if (count($mail->getBccAddresses()) >= 1) {
            foreach ($mail->getBccAddresses() as $address) {
                $mailer->addBCC($address);
            }
        }
        
        $mailer->Subject = $mail->getSubject();
        $mailer->MsgHTML($mail->getMessage());
        $mailer->AltBody = $mail->getMessage();
        $mailer->IsHTML(false);
        
        if (!$mailer->send()) {
            //FIXME: ESTE RETURN TEM DE SER COMENTADO E O QUE ESTA COMENTADO TEM DE SER DESCOMENTADO
            return true;
            //$ex = new ErrorSendingEmailException($mailer->ErrorInfo);
            //$this->log->error($ex);
            //throw $ex;
        }
        
        return true;
    }
    private static $instance;

    private function __construct() {
        $this->log = Logger::getLogger("log");
        $this->charSet = 'UTF-8';
    }

    public static function getInstance() {
        if (!isset(self::$instance)) {
            self::$instance = new MailServiceImpl();
        }
        return self::$instance;
    }
}

?>