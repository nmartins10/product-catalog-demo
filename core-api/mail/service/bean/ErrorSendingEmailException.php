<?php

class ErrorSendingEmailException extends Exception {

	private $description;

	public function __construct($description) {
		parent::__construct("description: " . $description);
		$this->description = $description;
	}

	public function getDescription() {
		return $this->description;
	}
	
}

?>