<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/mail/util/MailUtil.php';

/**
 * Description of Mail
 *
 * @author Nuno
 */
class Mail {
    
    private $from;
    private $fromName;
    private $subject;
    private $toAddresses;
    private $ccAddresses;
    private $bccAddresses;
    private $message;
    
    public function __construct() {
    	$this->toAddresses = array();
    	$this->ccAddresses = array();
    	$this->bccAddresses = array();
    }
    
    public function addToAddress($toAddress) {
    	$this->toAddresses[] = $toAddress;
    }
    
    public function addCcAddress($ccAddress) {
    	$this->ccAddresses[] = $ccAddress;
    }
    
	public function addBccAddress($bccAddress) {
    	$this->bccAddresses[] = $bccAddress;
    }
    
    public function getFrom() {
    	return $this->from;
    }
    
    public function setFrom($from) {
    	$this->from = $from;
    }
    
    public function getFromName() {
    	return $this->fromName;
    }
    
    public function setFromName($fromName) {
    	$this->fromName = $fromName;
    }
    
    public function getSubject() {
    	return $this->subject;
    }
    
    public function setSubject($subject) {
    	$this->subject = $subject;
    }
    
    public function getToAddresses() {
    	return $this->toAddresses;
    }
    
    public function setToAddresses(array $toAddresses) {
    	$this->toAddresses = $toAddresses;
    }
    
    public function getCcAddresses() {
    	return $this->ccAddresses;
    }
    
    public function setCcAddresses(array $ccAddresses) {
    	$this->ccAddresses = $ccAddresses;
    }
    
    public function getBccAddresses() {
    	return $this->bccAddresses;
    }
    
    public function setBccAddresses(array $bccAddresses) {
    	$this->bccAddresses = $bccAddresses;
    }
    
    public function getMessage() {
    	return $this->message;
    }
    
    public function setMessage($message) {
    	$this->message = $message;
    }
    
    public function __toString() {
    	return MailUtil::mailToString($this);
    }
}

?>
