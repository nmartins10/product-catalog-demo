<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/mail/service/bean/Mail.php';

abstract class MailUtil {
	
	public static function mailToString(Mail $mail) {
		$str = "from=" . $mail->getFrom() . ", fromName=" . $mail->getFromName();
		
		$totalRecords = count($mail->getToAddresses());
		if($totalRecords >= 1) {
			$str .= ', toAddresses=[';
			foreach ($mail->getToAddresses() as $address) {
				$count = 1;
				if($count != $totalRecords) {
					$str .= $address . ',';
				} else {
					$str .= $address . '],';
				}
				$count++;

			}
		} else {
			$str .= 'toAddresses=null,';
		}
		
		$totalRecords = count($mail->getCcAddresses());
		if($totalRecords >= 1) {
			$str .= ', ccAddresses=[';
			foreach ($mail->getCcAddresses() as $address) {
				$count = 1;
				if($count != $totalRecords) {
					$str .= $address . ',';
				} else {
					$str .= $address . '],';
				}
				$count++;
		
			}
		} else {
			$str .= 'ccAddresses=null,';
		}
		
		$totalRecords = count($mail->getBccAddresses());
		if($totalRecords >= 1) {
			$str .= ', bccAddresses=[';
			foreach ($mail->getBccAddresses() as $address) {
				$count = 1;
				if($count != $totalRecords) {
					$str .= $address . ',';
				} else {
					$str .= $address . '],';
				}
				$count++;
		
			}
		} else {
			$str .= 'bccAddresses=null,';
		}
		
		$str .= " subject=" . $mail->getSubject() . ", message=" . $mail->getMessage();
		return $str;
	}
	
}

?>