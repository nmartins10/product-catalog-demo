<?php

/**
 *
 * @author Nuno
 */
interface SessionDao {

    public function findSessionById($sessionId);

    public function openSession($userId, DateTime $openDt, $status);
    
    public function updateSession(SessionEntity $session);
}

?>
