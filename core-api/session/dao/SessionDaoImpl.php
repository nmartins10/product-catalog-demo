<?php

require_once 'SessionDao.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/session/dao/SessionDaoImpl.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/database/service/DatabaseServiceImpl.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/session/domain/SessionEntity.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/session/domain/SessionStatus.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/commons/EntityUtil.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/session/util/SessionUtil.php';

/**
 * Description of SessionDaoImpl
 *
 * @author Nuno
 */
class SessionDaoImpl implements SessionDao {

    private $connection;
    private $databaseService;

    public function findSessionById($sessionId) {
        $query = "SELECT * FROM session WHERE id = :id";
        $stmt = $this->connection->prepare($query);
        $stmt->execute(
                array(':id' => $sessionId)
        );
        
        $row = $stmt->fetch();
        //Validar q só encontra 1!!!
        //Ver se isto resulta se n encontrar!
        //Ver se isto resulta se n encontrar!
        //Ver se isto resulta se n encontrar!
        if($row == null) {
            return null;
        }
        
        return SessionUtil::convertSessionFromDatabase($row);
    }

    public function openSession($userId, DateTime $openDt, $status) {
        $query = "INSERT INTO session(user_id, open_dt, status) VALUES(:user_id, :open_dt, :status)";
        $stmt = $this->connection->prepare($query);
        $result = $stmt->execute(
                array(':user_id' => $userId,
                    ':open_dt' => EntityUtil::formatDateTimeToDatabase($openDt),
                    ':status' => $status)
        );

        $session = null;
        if ($result != FALSE) {
            $session = new SessionEntity();
            $session->setId($this->connection->lastInsertId());
            $session->setUserId($userId);
            $session->setOpenDt($openDt);
            $session->setStatus($status);
        }
        return $session;
    }

    public function updateSession(\SessionEntity $session) {
        $query = "UPDATE session SET user_id = :user_id, open_dt = :open_dt, close_type = :close_type, close_dt = :close_dt, status = :status WHERE id = :id";
        $stmt = $this->connection->prepare($query);
        $result = $stmt->execute(
                array(':user_id' => $session->getUserId(),
                    ':open_dt' => EntityUtil::formatDateTimeToDatabase($session->getOpenDt()),
                    ':close_type' => $session->getCloseType(),
                    ':close_dt' => EntityUtil::formatDateTimeToDatabase($session->getCloseDt()),
                    ':status' => $session->getStatus(),
                    ':id' => $session->getId())
        );
        return $result;
    }

    private static $instance;

    private function __construct() {
        $this->databaseService = DatabaseServiceImpl::getInstance();
        $this->connection = $this->databaseService->getConnection();
    }

    public static function getInstance() {
        if (!isset(self::$instance)) {
            self::$instance = new SessionDaoImpl();
        }
        return self::$instance;
    }

}

?>
