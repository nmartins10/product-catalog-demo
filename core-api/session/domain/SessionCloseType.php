<?php

/**
 * Description of SessionCloseType
 *
 * @author Nuno
 */
abstract class SessionCloseType {
    const REQUESTED = 'REQUESTED';
    const EXPIRED = 'EXPIRED';
}

?>
