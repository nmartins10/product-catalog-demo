<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/session/util/SessionUtil.php';

/**
 * Description of SessionEntity
 *
 * @author Nuno
 */
class SessionEntity {
    private $id;
    private $userId;
    private $openDt;
    private $closeDt;
    private $closeType;
    private $status;
    
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getUserId() {
        return $this->userId;
    }

    public function setUserId($userId) {
        $this->userId = $userId;
    }

    public function getOpenDt() {
        return $this->openDt;
    }

    public function setOpenDt(DateTime $openDt) {
        $this->openDt = $openDt;
    }

    public function getCloseDt() {
        return $this->closeDt;
    }

    public function setCloseDt(DateTime $closeDt) {
        $this->closeDt = $closeDt;
    }

    public function getCloseType() {
        return $this->closeType;
    }

    public function setCloseType($closeType) {
        $this->closeType = $closeType;
    }

    public function getStatus() {
        return $this->status;
    }

    public function setStatus($status) {
        $this->status = $status;
    }

    public function __toString() {
        return SessionUtil::sessionToString($this);
    }

}

?>
