<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/session/domain/SessionCloseType.php';

/**
 *
 * @author Nuno
 */
interface SessionService {
    
    public function openSession($userId);
    
    public function closeSession($sessionId, $closeType);
}

?>
