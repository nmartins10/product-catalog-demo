<?php
require_once 'SessionService.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/session/dao/SessionDaoImpl.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/time/service/LocalSystemTimeService.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/commons/EntityUtil.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/commons/Assert.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/session/domain/SessionStatus.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/session/domain/SessionCloseType.php';

/**
 * Description of SessionServiceImpl
 *
 * @author Nuno
 */
class SessionServiceImpl implements SessionService {
    
    private $timeService;
    private $sessionDao;
    
    private $log;
    
    public function openSession($userId) {
        $this->log->info("{userId=$userId}");
        Assert::notNull($userId, "userId is required and must be set");
        $openDt = $this->timeService->getCurrentDateTime();
        $status = SessionStatus::OPEN;
        
        return $this->sessionDao->openSession($userId, $openDt, $status);
    }
    
    public function closeSession($sessionId, $closeType) {
        $this->log->info("{sessionId=$sessionId, closeType=$closeType}");
        Assert::notNull($sessionId, "sessionId is required and must not be null");
        Assert::notNull($closeType, "closeType is required and must not be null");
        
        $session = $this->sessionDao->findSessionById($sessionId);
        if($session == null) {
            $ex = new SessionNotFoundException($sessionId);
            $this->log->error($ex);
            throw $ex;
        }
        
        $session->setCloseDt($this->timeService->getCurrentDateTime());
        $session->setCloseType($closeType);
        $session->setStatus(SessionStatus::CLOSED);
        
        $this->sessionDao->updateSession($session);
    }

    private static $instance;
    
    private function __construct() {
        $this->timeService = LocalSystemTimeService::getInstance();
        $this->sessionDao = SessionDaoImpl::getInstance();
        $this->log = Logger::getLogger("log");
    }
    
    public static function getInstance() {
        if(!isset(self::$instance)) {
            self::$instance = new SessionServiceImpl();
        }
        return self::$instance;
    }

}

?>
