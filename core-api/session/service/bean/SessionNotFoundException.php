<?php

/**
 * Description of SessionNotFoundException
 *
 * @author Nuno
 */
class SessionNotFoundException extends Exception {
    
    private $sessionId;
    
    public function __construct($sessionId) {
        parent::__construct("sessionId: " . $sessionId);
        $this->sessionId = $sessionId;
    }
    
    public function getSessionId() {
        return $this->sessionId;
    }

}

?>
