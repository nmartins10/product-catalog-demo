<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/session/domain/SessionEntity.php';

/**
 * Description of SessionUtil
 *
 * @author Nuno
 */
abstract class SessionUtil {
    
    public static function convertSessionFromDatabase($databaseRow) {
        $session = new SessionEntity();
        $session->setId($databaseRow['id']);
        $session->setUserId($databaseRow['user_id']);
        $session->setOpenDt(new DateTime($databaseRow['open_dt']));
        if($session->getCloseDt() != null) {
	        $session->setCloseDt(new DateTime($databaseRow['close_dt']));
        }
        $session->setCloseType($databaseRow['close_type']);     //ver se isto funciona assim directamente
        $session->setStatus($databaseRow['status']);        //ver se isto funciona assim directamente
        
        return $session;
    }
    
    public static function sessionToString(SessionEntity $session) {
        $str = "id=" . $session->getId() . ", userId=" . $session->getUserId() . ",";
        
        if($session->getOpenDt() != null) {
            $str .= " openDt=" . EntityUtil::formatDateTimeToDatabase($session->getOpenDt()) . ",";
        } else {
            $str .= " openDt=null,";
        }
        
        if($session->getCloseDt() != null) {
            $str .= " closeDt=" . EntityUtil::formatDateTimeToDatabase($session->getCloseDt()) . ",";
        } else {
            $str .= " closeDt=null,";
        }
        
        $str .= " closeType=" . $session->getCloseType() . ", status=" . $session->getStatus();
        
        return $str;
    }
}

?>
