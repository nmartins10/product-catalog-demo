<?php
require_once 'TimeService.php';

/**
 * Description of LocalSystemTimeService
 *
 * @author Nuno
 */
class LocalSystemTimeService implements TimeService {
    
    public function getCurrentDateTime() {        
        return new DateTime('NOW');
    }
    
    private static $instance;
    
    private function __construct() {
        date_default_timezone_set('Europe/Lisbon');
    }
    
    public static function getInstance() {
        if(!isset(self::$instance)) {
            self::$instance = new LocalSystemTimeService();
        }
        return self::$instance;
    }
}

?>
