<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/export-api/authentication/service/bean/ExportCredentials.php';

interface AuthenticationExportService {
    
    public function authenticate(ExportCredentials $credentials);     //TODO: Criar Credentials.php no export? Ser� q como j� existe no core funciona???
    
    public function logout($sessiondId, $closeType);
    
    public function hasSessionTimedOut();
    
    public function hashPassword($password);
    
    public function expireSession();
}

?>