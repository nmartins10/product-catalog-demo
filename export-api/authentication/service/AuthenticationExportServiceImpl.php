<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/export-api/authentication/service/AuthenticationExportService.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/export-api/authentication/service/bean/ExportCredentials.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/authentication/service/bean/Authentication.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/authentication/service/AuthenticationServiceImpl.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/export-api/authentication/service/bean/ExportAuthentication.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/export-api/authentication/service/bean/ExportAuthentication.php';

class AuthenticationExportServiceImpl implements AuthenticationExportService {
    private static $SALT = "�~^%|!#)=?�@�-_$;{[]}�+*�`&(.:,��";
    private static $DEFAULT_SESSION_TIMEOUT = 3600;
    private $authenticationService;
    private $log;

    public function authenticate(ExportCredentials $credentials) {
        $this->log->info("{email=" . $credentials->getEmail() . "}");
        $coreCredentials = $this->convertCredentialsFromExportToCore($credentials);
        
        $coreAuthentication = $this->authenticationService->authenticate($coreCredentials);
        return $this->convertAuthenticationFromCoreToExport($coreAuthentication);
    }

    public function logout($sessiondId, $closeType) {
        $this->log->info("{sessionId=$sessiondId, closeType=$closeType }");
        $this->authenticationService->logout($sessionId, $closeType);
    }

    public function hashPassword($password) {
        return sha1($password . self::$SALT);
    }

    public function hasSessionTimedOut() {
        if (isset($_SESSION['timeout'])) {
            $session_life = time() - $_SESSION['timeout'];
            if ($session_life > $time_limit) {
                $vars = array(
                    'timeout' => 1);
                return true;
            }
        }
        $_SESSION['timeout'] = time();
        return false;
    }

    public function expireSession() {
        session_destroy();
    }

    private function convertCredentialsFromExportToCore(ExportCredentials $credentials) {
        $coreCredentials = null;
        if ($credentials != null) {
            $coreCredentials = new Credentials($credentials->getEmail(), $credentials->getPasswordHash());
        }
        return $coreCredentials;
    }
    
    private function convertAuthenticationFromCoreToExport(Authentication $coreAuthentication) {
        $exportAuthentication = null;
        if ($coreAuthentication != null) {
            $exportAuthentication = new ExportAuthentication($coreAuthentication->getUser(), $coreAuthentication->getSession());
        }
        return $exportAuthentication;
    }

    private static $instance;
    
    private function __construct() {
        $this->log = Logger::getLogger("log");
        $this->authenticationService = AuthenticationServiceImpl::getInstance();
    }

    public static function getInstance() {
        if (!isset(self::$instance)) {
            self::$instance = new AuthenticationExportServiceImpl();
        }
        return self::$instance;
    }
}

?>