<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/export-api/authentication/service/bean/UserTypeExport.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/export-api/authentication/service/bean/User.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/export-api/authentication/service/bean/UserSearchCriteriaExport.php';

interface UserManagementExportService {

    public function getUser($userId);

    public function searchUsers(UserSearchCriteriaExport $searchCriteria);

    public function createUser($email, $passwordHash, $name, UserTypeExport $type);

    public function updateUser(User $user);

    public function deleteUser($userId);

    public function blockUser($userId);

    public function unblockUser($userId);

    public function userHasValidPasswordRecovery($email);

    public function recoverPassword($email);

    public function confirmPasswordRecovery($email, $recoveryCode, $newPasswordHash);

}

?>