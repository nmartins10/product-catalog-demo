<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/export-api/authentication/service/UserManagementExportService.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/authentication/service/UserManagementService.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/authentication/service/UserManagementServiceImpl.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/export-api/authentication/service/bean/UserSearchCriteriaExport.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/authentication/service/bean/UserSearchCriteria.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/export-api/authentication/service/bean/User.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/ProductCatalogDemo/core-api/authentication/domain/UserEntity.php';

class UserManagementExportServiceImpl implements UserManagementExportService {
    private $userManagementService;

    public function getUser($userId) {
    	$userEntity = $this->userManagementService->getUser($userId);
    	return UserConverter::convertUserEntity2User($userEntity);
    }

    public function searchUsers(UserSearchCriteriaExport $searchCriteria) {}

    public function createUser($email, $passwordHash, $name, UserTypeExport $type) {}

    public function updateUser(User $user) {}

    public function deleteUser($userId) {}

    public function blockUser($userId) {}

    public function unblockUser($userId) {}

    public function userHasValidPasswordRecovery($email) {}

    public function recoverPassword($email) {}

    public function confirmPasswordRecovery($email, $recoveryCode, $newPasswordHash) {}
    private static $instance;

    private function __construct() {
        $this->$userManagementService = UserManagementServiceImpl::getInstance();
    }

    public static function getInstance() {
        if (!isset(self::$instance)) {
            self::$instance = new UserManagementExportServiceImpl();
        }
        return self::$instance;
    }
}

?>