<?php
class ExportAuthentication {
    private $user;
    private $session;

    public function __construct($user, $session) {
        $this->user = $user;
        $this->session = $session;
    }
    
    public function getUser() {
        return $this->user;
    }

    public function setUser($user) {
        $this->user = $user;
    }

    public function getSession() {
        return $this->session;
    }

    public function setSession($session) {
        $this->session = $session;
    }


}
?>